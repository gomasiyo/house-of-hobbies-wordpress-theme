<?php get_header(); ?>
    <section id="main">
    
        <?php if(!is_home()): ?>
            <?php breadcrumb(); ?>
        <?php endif; ?>
             
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            
                <article class="posts">
                    <header>
                        <h1><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
                    </header>
                    <div>
    					<?php if ( has_post_thumbnail() ) { 
    						$img_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
    						list( $src, $width, $height )  = $img_url;?>
    						<p><a href="<?php echo $src; ?>"><img  src="<?php echo $src; ?>" class="eye_catch"/></a></p>
    					<?php  } ?>
                        <?php the_content('続きを読む'); ?>
                    </div>
                    <br class="cancel">
                    <footer>
                        <ul>
                            <li>Post: <?php the_time('Y年m月d日') ?></li>
                            <li>Category: <?php the_category(', ') ?></li>
                            <li>Posted: by <?php the_author(); ?></li>
                        </ul>
                        <br class="cancel">
                    </footer>
                </article>
            
            <?php endwhile; ?>
            
            <footer id="post_footer">
                <nav>
                    <ul>
                        <li id="post_footer_left"><?php next_posts_link('古い記事へ') ?></li>
                        <li id="post_footer_right"><?php previous_posts_link('新しい記事へ') ?></li>
                    </ul>
                </nav>
            </footer>
            
            <?php else : ?>
            
                <article class="posts">
                    <header>
                        <h1>記事が見つかりませんでした。</h1>
                    </header>
                    <div>
                        <p>検索で見つかるかもしれません。</p>
                        <p><?php get_search_form(); ?></p>
                    </div>
                    <br class="cancel">
                </article>
        
            <?php endif; ?>

    </section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>