<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><?php wp_title ( '-', true, 'right'); ?><?php bloginfo('name'); ?></title>
<!-- jQuery Faster Tags. -->
<?php
    wp_deregister_script('jquery');
        wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js', array(), '1.7.2');
?>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/dist/html5shiv.js"></script>
<![endif]-->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSSフィード" href="<?php bloginfo('rss2_url'); ?>">
<?php wp_head(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/script.php"></script>
</head>
<body <?php body_class(); ?>>

<header id="header">
    <div id="header_logo">
        <img src="<?php bloginfo('template_url'); ?>/images/header_logo.png">
    </div>
    <div id="header_nav">
        <div id="header_nav_center">
            <nav>
                <?php wp_nav_menu(); ?>
            </nav>
        </div>
    </div>
    <br class="cancel">
</header>

<section id="main_page">