<?php get_header(); ?>
    <section id="main">
    
        <?php if(!is_home()): ?>
            <?php breadcrumb(); ?>
        <?php endif; ?>
        
                            
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            
                <article class="posts">
                    <header>
                        <h1><?php the_title(); ?></h1>
                    </header>
                    <div>
    					<?php the_content(); ?>
                    </div>
                    <br class="cancel">
                    <footer>
                        <ul>
                            <li>Post: <?php the_time('Y年m月d日') ?></li>
                            <li>Posted: by <?php the_author(); ?></li>
                        </ul>
                        <br class="cancel">
                    </footer>
                    
                </article>
            
            <?php endwhile; ?>
            
            <?php else : ?>
            
                <article class="posts">
                    <header>
                        <h1>記事が見つかりませんでした。</h1>
                    </header>
                    <div>
                        <p>検索で見つかるかもしれません。</p>
                        <p><?php get_search_form(); ?></p>
                    </div>
                    <br class="cancel">
                </article>
        
            <?php endif; ?>

    </section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>