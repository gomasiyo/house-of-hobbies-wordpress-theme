<?php get_header(); ?>
    <section id="main">
    
        <?php if(!is_home()): ?>
            <?php breadcrumb(); ?>
        <?php endif; ?>
        
                            
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            
                <article class="posts">
                    <header>
                        <h1><?php the_title(); ?></h1>
                    </header>
                    <div>
    					<?php the_content(); ?>
                    </div>
                    <br class="cancel">
                    <footer>
                        <ul>
                            <li>Post: <?php the_time('Y年m月d日') ?></li>
                            <li>Category: <?php the_category(', ') ?></li>
                            <li>Posted: by <?php the_author(); ?></li>
                        </ul>
                        <br class="cancel">
                    </footer>
                    
                    <?php comments_template(); ?>
                    
                </article>
            
            <?php endwhile; ?>
            
            <footer id="post_footer">
                <nav>
                    <ul>
                        <li id="post_footer_left"><?php previous_post_link('%link', '古い記事へ'); ?></li>
                        <li id="post_footer_right"><?php next_post_link('%link', '新しい記事へ'); ?></li>
                    </ul>
                </nav>
            </footer>
            
            <?php else : ?>
            
                <article class="posts">
                    <header>
                        <h1>記事が見つかりませんでした。</h1>
                    </header>
                    <div>
                        <p>検索で見つかるかもしれません。</p>
                        <p><?php get_search_form(); ?></p>
                    </div>
                    <br class="cancel">
                </article>
        
            <?php endif; ?>

    </section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>